<?php
/**
 * @file
 * Advanced user module allows you to select users based on an advanced set of
 * filtering and apply actions to block, unblock, delete or email the selected
 * users.
 *
 **/

/**
 * @defgroup advuser_constants Advanced User constants
 * @{
 */
define('ADVUSER_SUBSTITUTION_TEXT', '<strong>Substitution variables</strong> available in subject and email body<br/><em> %user_name, %user_email, %user_status, %user_created, %user_theme, %user_timezone, %user_language, %user_signature, %site, %uri</em>');

define('ADVUSER_DEFAULT_NEW_SUBJECT', '[%site] On %user_created a new user registered as %user_name.');

define('ADVUSER_DEFAULT_NEW_MAIL', "=====  New User Information:  =====
    User: %user_name 
    Mail: %user_email
 Created: %user_created 
  Status: %user_status
Timezone: %user_timezone
Language: %user_language
   Theme: %user_theme

Administor user at %uri for %site");

define('ADVUSER_DEFAULT_MODIFY_SUBJECT', '[%site] User %user_name has modified their account.');

define('ADVUSER_DEFAULT_MODIFY_MAIL', "=====  Updated User Information:  =====
    User: %user_name 
    Mail: %user_email
 Created: %user_created 
  Status: %user_status
Timezone: %user_timezone
Language: %user_language
   Theme: %user_theme

Administor user at %uri for %site");

/**
 * @} End of "defgroup advuser_constants".
 */

/**
 * @defgroup advuser_hook Advanced User hook implementations
 * @{
 * Functions in this group are implementations of Drupal hooks.  Each function
 * should provide it's \@ingroup advuser_hook.
 * @} End of "defgroup advuser_hook".
 */

/**
 * @ingroup advuser_hook
 * Implementation of hook_init().
 */
function advuser_init() {
  drupal_add_css(drupal_get_path('module', 'advuser') . '/css/advuser.css', 'module');

  // The persistent data for the module is stored in the session.
  $advuser = &$_SESSION['advuser'];
  if (!isset($advuser)) {
    $advuser = array(
      'accounts' => array(),
      'deselected' => array(),
      'filters' => array(),
      'phase' => array('id' => 0),
      'return' => FALSE,
      'selectall' => FALSE,
    );
  }
}

/**
 * @ingroup advuser_hook
 * Implementation of hook_help().
 */
function advuser_help($path, $arg) {
  $output = NULL;
  switch ($path) {
    case 'admin/help#advuser': {
      $output = '<p>';
      $output .= t('This module provides greater control of filtering of the
users and allows for persistent selection of users between multiple pages of
user data.  Besides the normal actions of block, unblock, delete, role
assignment and role unassignment you can also select all filtered users,
deselect all users and email the selected users.');
      $output .= '</p><p>';
      $output .= t('The filtering selection is a multipart form where you
first select the field you want to filter by.  Lets use the mail address for
example as the selected field.  You then press Continue and you choose the
operator for the data that you enter then you click Filter.  If you chose the
wrong field you can go back to the field selection by clicking the Back button.
Once you have one filter in place you can refine the filter with and/or
conjunctors and another field type or the same field type.');
      $output .= '</p>';
    } break;
    case 'admin/user/user/advuser': {
      $output = '<p>';
      $output .= t('Advanced User module provides filtering of users with 
persistent selections of users between mulitple pages of the users list.  The
module also adds an action for email to be sent to the selected user set as
well as the normal actions already available.');
      $output .= '</p>';
    } break;
  }
  return $output;
}

/**
 * @ingroup advuser_hook
 * Implementation of hook_perm().
 */
function advuser_perm() {
  return array(
    'administer advuser',
    'access advuser',
    'send email advuser',
    'receive email advuser',
  );
}

/**
 * @ingroup advuser_hook
 * Implementation of hook_menu().
 */
function advuser_menu() {

  $items['admin/settings/advuser'] = array(
    'title' => t('Advanced User'),
    'description' => t('Advanced User Settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('advuser_settings'),
    'access arguments' => array('administer advuser'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'forms/advuser_settings.inc',
  );

  $items['admin/user/user/advuser'] = array(
    'title' => t('Advanced'),
    'description' => t('List, add, edit and email users.'),
    'page callback' => 'advuser_admin', 
    'page arguments' => array('list'), 
    'access callback' => 'advuser_admin_access',
    'access arguments' => array('access advuser', 'administer users'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'forms/advuser_admin.inc',
  );

  $items['admin/user/user/advuser/confirm/email'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('advuser_multiple_email_confirm'),
    'access callback' => 'advuser_admin_access',
    'access arguments' => array('access advuser', 'administer users', 'send email advuser'),
    'type' => MENU_CALLBACK,
    'file' => 'forms/advuser_multiple_email_confirm.inc'
  );

  $items['admin/user/user/advuser/confirm/delete'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('advuser_multiple_delete_confirm'),
    'access callback' => 'advuser_admin_access',
    'access arguments' => array('access advuser', 'administer users'),
    'type' => MENU_CALLBACK,
    'file' => 'forms/advuser_multiple_delete_confirm.inc'
  );

  return $items;
}

/**
 * Access callback for menu items.
 */
function advuser_admin_access () {
  $accesses = func_get_args();
  if (empty($accesses)) {
    $accesses = array('access advuser');
  }
  foreach ($accesses as $access) {
    $ret = user_access($access);
    if (!$ret) {
      break;
    }
  }
  return $ret;
}

/**
 * @ingroup advuser_hook
 * hook_theme implementation  
 */
function advuser_theme() {
  return array(
    'advuser_admin_account' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

/**
 * @ingroup advuser_advuser_hook
 * Implement a hook_advuser_operations function.
 */
function advuser_advuser_operations($form_state = array()) {
  $operations = array(
    'saveselect' => array(
      'label' => t('Save selection criteria'),
    ),
    'selectall' => array(
      'label' => t('Select all filtered users'),
    ),
    'deselectall' => array(
      'label' => t('Deselect all users'),
    ),
    'email' => array(
      'label' => t('Email selected users'),
    ),
    'block' => array(
      'label' => t('Block the selected users'),
    ),
    'unblock' => array(
      'label' => t('Unblock the selected users'),
    ),
    'delete' => array(
      'label' => t('Delete the selected users'),
    ),
  );

  // Copied directly from user.module user_user_operations.
  if (user_access('administer permissions')) {
    $roles = user_roles(TRUE);
    unset($roles[DRUPAL_AUTHENTICATED_RID]);  // Can't edit authenticated role.

    $add_roles = array();
    foreach ($roles as $key => $value) {
      $add_roles['add_role-'. $key] = $value;
    }

    $remove_roles = array();
    foreach ($roles as $key => $value) {
      $remove_roles['remove_role-'. $key] = $value;
    }

    if (count($roles)) {
      $role_operations = array(
        t('Add a role to the selected users') => array(
          'label' => $add_roles,
        ),
        t('Remove a role from the selected users') => array(
          'label' => $remove_roles,
        ),
      );

      $operations += $role_operations;
    }
  }

  // If the form has been posted, we need to insert the proper data for
  // role editing if necessary.
  if (!empty($form_state['submitted'])) {
    $operation_rid = explode('-', $form_state['values']['operation']);
    $operation = $operation_rid[0];
    if ($operation == 'add_role' || $operation == 'remove_role') {
      $rid = $operation_rid[1];
      if (user_access('administer permissions')) {
        $operations[$form_state['values']['operation']] = array(
          'callback' => 'advuser_multiple_role_edit',
          'callback arguments' => array($operation, $rid),
        );
      }
      else {
        watchdog('security', 'Detected malicious attempt to alter protected user fields.', array(), WATCHDOG_WARNING);
        return;
      }
    }
  }
  return $operations;
}

/**
 * @ingroup advuser_hook
 * Implementation of hook_mail
 */
function advuser_mail($key, &$message, $params) {
  $message = array_merge($message, $params);
}

/**
 * Get a list of substitution variables for the user account
 * @param $user the user account
 * @return An associative array of substitution variables 
 */
function _advuser_get_variables($user) {
  return array(
    '%user_name' => $user->name,
    '%site' => variable_get("site_name", "drupal"), 
    '%uri' => url('user/'. $user->uid, array('absolute' => TRUE)), 
    '%user_email' => $user->mail,
    '%user_status' => t($user->status ? 'Active' : 'Blocked'),
    '%user_theme' => empty($user->theme) ? t('DEFAULT') : $user->theme,
    '%user_created' => strftime('%x %X', $user->created),
    '%user_language' => empty($user->language) ? t('DEFAULT') : $user->language,
    '%user_timezone' => empty($user->timezone) ? '0' : "$user->timezone",
    '%user_signature' => $user->signature,
  );
}

/**
 * @private
 * Return a list of users to send notification of user changes.
 *
 * @return resource         // The result of the db_query.
 */
function _advuser_dbquery_users_to_notify() {
  $user_where = users_by_access('receive email advuser');
  $user_where = implode(',', $user_where);
  return empty($user_where) ? FALSE : db_query('SELECT u.* FROM {users} u WHERE u.uid IN (' . $user_where . ')');
}

/**
 * @public
 * List of users for given roles
 *
 * @param string $perm                  // The string permission.
 * @return array                        // An array of uid.
 */
function users_by_access($perm) {
  $select_permissions = "SELECT ur.uid FROM {permission} p LEFT JOIN {users_roles} ur ON ur.rid = p.rid WHERE p.perm like '%%%s%%'";
  $ret = array();
  $result = db_query($select_permissions, $perm);
  while ($data = db_fetch_object($result)) {
    if (isset($data->uid)) {
      $ret[] = (int)$data->uid;
    }
  }
  if (!in_array(1, $ret, TRUE)) {
    if (variable_get('advuser_notify_uid1', FALSE)) {
      $ret[] = 1;
    }
  }
  return $ret;
}

/**
 * @private
 * Notify account registrations and modifications for the given users of a role.
 *
 * @param string $op
 * @param object $account
 */
function _advuser_receive_notification_by_role ($op, $account) {
  static $accounts = array();

  $from = variable_get("site_mail", ini_get("sendmail_from"));

  switch ($op) {
    case 'insert': {
      $notify = variable_get('advuser_new_notify', FALSE) &&
              ! user_access('administer users');
      $body = variable_get('advuser_new_mail', t(ADVUSER_DEFAULT_NEW_MAIL));
      $subject = variable_get('advuser_new_subject', t(ADVUSER_DEFAULT_NEW_SUBJECT));
    } break;
    case 'update': {
      $notify = variable_get('advuser_modify_notify', FALSE) &&
              ! user_access('administer users');
      $body = variable_get('advuser_modify_mail', t(ADVUSER_DEFAULT_NEW_MAIL));
      $subject = variable_get('advuser_modify_subject', t(ADVUSER_DEFAULT_NEW_SUBJECT));
    } break;
  }

  $variables = _advuser_get_variables($account);
  $subject = strtr($subject, $variables);
  $body = strtr($body, $variables);

  if ($notify) {
    if (variable_get('advuser_log_notifications', FALSE)) {
      watchdog('advuser', "Sending notification mail: from='$from' subj='$subject' body='$body'");
    }

    if (empty($accounts)) {
      $result = _advuser_dbquery_users_to_notify();
      while ($row = db_fetch_object($result)) {
        $accounts[] = $row;
      }
    }

    foreach ($accounts as $account) {
      drupal_mail('advuser', 'advanced-user-mail', $account->mail, user_preferred_language($account), array('subject' => $subject, 'body' => $body), $from, TRUE);
    }
  }
}

/** 
 * @ingroup advuser_hook
 * hook_user implementation  
 *
 * On 'form' (indicates user edit form) save the account data in a static cache.
 * On 'insert' call the registration notification function.
 * On 'after_update' call the profile update notification function.
 */
function advuser_user($type, &$edit, &$account, $category = NULL) {
  static $account_before_edit;
  static $insert = FALSE;
  $return = NULL;
  switch ($type) {
    case 'load': {
      if ($insert) {
        // Now the record exists and we can update it.
        if (variable_get('advuser_set_never_access', FALSE)) {
          if ($account->login == 0) {
            $account->access = 0;
            _advuser_update_never_accessed($account->uid);
          }
        }
        $insert = FALSE;
      }
    } // continue into the 'form' case.
    case 'form': {
      // Need to remove the form identification data!
      // Why does it need to be stored in the data serialized array?!
      $account_before_edit = $account;
      $account_before_edit->form_build_id = '';
      if (is_string($account->data)) {
        $account_before_edit->data = unserialize($account->data);
      }
      unset ($account_before_edit->data['form_build_id']);
    } break;
    case 'insert': {
      // Flag that we've seen the insert operation. The insert hasn't occurred
      // but is being readied for insert.
      $insert = TRUE;
      // Notify those that need to be.
      $return = _advuser_receive_notification_by_role('insert', $account);
    } break;
    case 'after_update': {
      // Need to remove the form identification data!
      // Why does it need to be stored in the data serialized array?!
      $account_after_edit = $account;
      $account_after_edit->form_build_id = '';
      if (is_string($account_after_edit->data)) {
        $account_after_edit->data = unserialize($account->data);
      }
      unset ($account_after_edit->data['form_build_id']);
      if ($account_after_edit->login == 0) {
        if (variable_get('advuser_reset_never_access', FALSE)) {
          $account_after_edit->access = 0;
          _advuser_update_never_accessed($account_after_edit->uid);
        }
      }
      if ($account_after_edit != $account_before_edit) {
        $return = _advuser_receive_notification_by_role('update', $account);
      }
    } break;
  }
  return $return;
}

function _advuser_update_never_accessed($uid) {
  global $user;
  if ($user->uid != $uid && user_access('administer users')) {
    $sql = 'UPDATE {users} SET access = 0 WHERE uid = %d';
    return db_query($sql, $uid);
  }
}

/**
 * Selected Profile Fields
 * @return array
 */
function advuser_profile_fields() {
  static $ret = array();
  if (!count($ret) && module_exists('profile')) {
    $fields = variable_get('advuser_profile_fields', NULL);
    if (is_array($fields)) {
      foreach ( $fields as $fid => $value) {
        if ( $value ) {
          $ret[]= db_fetch_object(db_query('SELECT * FROM {profile_fields} WHERE fid = %d', $fid));
        }
      }
    }
  }
  return $ret;
}

/**
 * Profile Field Values
 *
 * @param int $fid
 * @param int $uid
 * @return mixed
 */
function advuser_profile_value($fid, $uid) {
  $ret = db_result(db_query("SELECT value FROM {profile_values} WHERE fid = %d and uid = %d", $fid, $uid));
  if ($ret === FALSE) {
    $ret = NULL;
  }
  return $ret;
}

/**
 * User Roles for use in ADVUSER.
 *
 * @return array
 */
function advuser_user_roles() {
  static $roles = array();
  if (empty($roles)) {
    $roles = user_roles(TRUE);
    unset ($roles[DRUPAL_AUTHENTICATED_RID]);
  }
  return $roles;
}

/**
 * Get the sql query string.
 *
 * @param $type
 * @return The sql string.
 * - $type = 'users' returns a select string that include uid, name, mail,
 *   status, created and access.
 * - $type = 'name' returns a select string that includes uid and name.
 * - $type = 'count' returns a select string for the count for the list.
 * - $type = 'uid' returns a select string that includes the uid.
 */
function advuser_build_query ($type = 'users') {
  $filter = advuser_build_filter_query();

  // Array used for user profile DB query joins.
  $pf = array();

  foreach (advuser_profile_fields() as $field) {
    if (isset($field->name)) {
      $pf[] = "LEFT JOIN {profile_values} $field->name ON $field->name.fid = $field->fid AND $field->name.uid = u.uid";
    }
  }

  $filter['join'] .= count($pf) ? ' ' . implode(' ', array_unique($pf)) : NULL;

  switch ($type) {
    case 'users': {
      $sql = 'SELECT DISTINCT u.uid, u.name, u.mail, u.status, u.created, u.access FROM {users} u LEFT JOIN {users_roles} ur ON u.uid = ur.uid '. $filter['join'];
    } break;
    case 'uid': {
      $sql = 'SELECT DISTINCT u.uid as uid FROM {users} u LEFT JOIN {users_roles} ur ON u.uid = ur.uid '. $filter['join'];
    } break;
    case 'name': {
      $sql = 'SELECT DISTINCT u.uid, u.name FROM {users} u LEFT JOIN {users_roles} ur ON u.uid = ur.uid ' . $filter['join'];
    } break;
    case 'count': {
      $sql = 'SELECT COUNT(DISTINCT u.uid) FROM {users} u LEFT JOIN {users_roles} ur ON u.uid = ur.uid '. $filter['join'];
    } break;
  }

  return $sql . (empty($filter['where']) ? '' : ' WHERE ' . $filter['where']);
}

/**
 * Build query for advuser administration filters based on session.
 *
 * @param $reset Resets the static cached array.
 * @return array containing 'join' clauses, 'where' clauses and replacement
 *   'args' for the variable sections of the clauses.
 */
function advuser_build_filter_query($reset = FALSE) {
  $advuser = &$_SESSION['advuser'];
  static $filter_array = array();

  if (empty($filter_array) || $reset) {
    $filters = &$advuser['filters'];

    $advuser_filters = advuser_filters();

    // Build query
    $where = $args = $join = array();

    foreach ($filters as $filter) {
      list($key, $op, $qop, $value) = array_values($filter);
      // This checks to see if this permission filter is an enabled permission 
      // for the authenticated role.  If so, then all users would be listed, and 
      // we can skip adding it to the filter query.
      switch ($key) {
        case 'permission': {
          $account = new stdClass();
          $account->uid = 'advuser_filter';
          $account->roles = array(DRUPAL_AUTHENTICATED_RID => 1);
          if (user_access($value, $account)) {
            continue;
          }
        } break;
        case 'created':
        case 'last_access': {
          // Convert the string date to a unix time value.
          $value = strtotime($value);
        } break;
        case 'user_roles': {
          // Allow for filtering for users where no role is set.
          if (ord($value) === 0) {
            $qop = 'IS';
            $value = 'NULL';
          }
        } break;
      }

      // Provide for '%%%s%%', '%%%s' and '%s%%' as needed.
      $arg_prefix = $arg_suffix = NULL;
      switch ($qop) {
        case 'NOT LIKE':
        case 'LIKE': {
          $arg_prefix = $arg_suffix = '%';
        } break;
        case 'BEGINS WITH': {
          $qop = 'LIKE';
          $arg_suffix = '%';
        } break;
        case 'ENDS WITH': {
          $qop = 'LIKE';
          $arg_prefix = '%';
        } break;
      }

      // Provide operator symbol to SQL translation.
      switch ($qop) {
        case '!=':
        case 'NOT LIKE': {
          $in = 'NOT IN';
          $eq = '!=';
          $andor = 'AND';
        } break;
        default: {
          $in = 'IN';
          $eq = '=';
          $andor = 'OR';
        }
      }

      // Build the 'where' clauses.
      $_where = $op.' '.str_ireplace("%op", $qop, $advuser_filters[$key]['where']);
      $_where = str_ireplace("%eq", $eq, $_where);
      $_where = str_ireplace("%andor", $andor, $_where);
      $where[] = str_ireplace("%in", $in, $_where);

      // Build the argument values.
      $args[] = $arg_prefix . $value . $arg_suffix;

      // Build the join clauses.
      $join[] = $advuser_filters[$key]['join'];
    }

    $where = count($where) ? '('. implode(' ', $where) . ')' : '';
    $join = count($join) ? ' '. implode(' ', array_unique($join)) : '';

    $filter_array = array(
      'where' => $where,
      'join' => $join,
      'args' => $args,
    );
  }

  return $filter_array;
}

/**
 * List advuser administration filters that can be applied.
 */
function advuser_filters() {
  // Regular filters
  static $filters = array();

  if (empty($filters)) {
    $options = array();
    $t_module = t('module');
    foreach (module_list() as $module) {
      if ($permissions = module_invoke($module, 'perm')) {
        asort($permissions);
        foreach ($permissions as $permission) {
          $options["$module $t_module"][$permission] = t($permission);
        }
      }
    }

    ksort($options);
    $filters['permission'] = array(
      'title' => t('Permission'),
      'where' => " ((u.uid %in (SELECT ur.uid FROM {users_roles} ur WHERE ur.rid %in (SELECT p.rid FROM {permission} p WHERE p.perm %op '%s'))) %andor u.uid %eq 1)",
      'options' => $options,
      'form_type' => 'select',
    );

    $filters['status'] = array(
      'title' => t('Status'),
      'where' => "u.status %op '%s'",
      'options' => array(1 => t('active'), 0 => t('blocked')),
      'form_type' => 'select',
    );

    $filters['created'] = array(
      'title' => t('Created'),
      'where' => "u.created %op %d",
      'form_type' => 'date',
    );

    $filters['last_access'] = array(
      'title' => t('Last Accessed'),
      'where' => "u.access %op %d",
      'form_type' => 'date',
    );

    $filters['email'] = array(
      'title' => t('Email'),
      'where' => "u.mail %op '%s'",
      'form_type' => 'textfield',
    );

    $filters['uid'] = array(
      'title' => t('User Id'),
      'where' => "u.uid %op %d",
      'form_type' => 'id',
    );

    $filters['username'] = array(
      'title' => t('Username'),
      'where' => "u.name %op '%s'",
      'form_type' => 'textfield',
    );

    $roles = advuser_user_roles();
    if (count($roles)) {
      $filters['user_roles'] = array(
        'title' => t('Role'),
        'where' => "ur.rid %op %s",
        'form_type' => 'select',
        'options' => $roles,
      );
    }

    $profile_fields = advuser_profile_fields();
    foreach ($profile_fields as $field) {
      // Build array of options if they exist
      $opts = NULL;
      if ( ! empty($field->options) ) {
        $opts = array();
        foreach ( explode("\n", $field->options) as $opt ) {
          $opt = trim($opt);
          $opts[$opt] = $opt;
        }
      }
      // Each user defined profile field needs a unique table identifier for the
      //  JOIN and WHERE clauses.
      // TODO: Make sure the $field->name contains valid information for a table
      //  identifier.  This comment is to identify the source of a problem yet 
      //  to be discovered.
      $pv = $field->name;
      $filters[$field->name] = array(
        'title' => check_plain($field->title),
        'type' => $field->type,
        'class' => $field->name,
        'where' => "$pv.value %op '%s' AND $pv.uid = u.uid",
        'options' => $opts,
        'autocomplete' => $field->autocomplete ? $field->fid : FALSE,
      );
    }
  }
  return $filters;
}

/**
 * Callback function for admin mass adding/deleting a user role.
 *
 * @param $accounts - Not used
 * @param $operation - add_role or remove_role
 * @param $rid - the requested role id.
 */
function advuser_multiple_role_edit($accounts, $operation, $rid) {
  // The role name is not necessary as user_save() will reload the user
  // object, but some modules' hook_user() may look at this first.
  $advuser = &$_SESSION['advuser'];

  $accounts = &$advuser['accounts'];
  $selectall = &$advuser['selectall'];
  $deselected = &$advuser['deselected'];

  $filter = advuser_build_filter_query();

  $role_name = db_result(db_query('SELECT name FROM {role} WHERE rid = %d', $rid));

  switch ($operation) {
    case 'add_role': {
      if ($selectall) {
        $sql = advuser_build_query('uid');
        $result = db_query($sql, $filter['args']);
        while ($user = db_fetch_array($result)) {
          $account = user_load($user);
          if ($account !== FALSE && !isset($account->roles[$rid])) {
            $roles = $account->roles + array($rid => $role_name);
            user_save($account, array('roles' => $roles));
          }
        }
        $selectall = FALSE;
      }
      else {
        foreach ($accounts as $uid) {
          $account = user_load(array('uid' => (int)$uid));
          // Skip adding the role to the user if they already have it.
          if ($account !== FALSE && !isset($account->roles[$rid])) {
            $roles = $account->roles + array($rid => $role_name);
            user_save($account, array('roles' => $roles));
          }
          unset($accounts[$uid]);
        }
      }
    } break;
    case 'remove_role': {
      if ($selectall) {
        $sql = advuser_build_query('uid');
        $result = db_query($sql, $filter['args']);
        while ($user = db_fetch_array($result)) {
          $account = user_load($user);
          if ($account !== FALSE && isset($account->roles[$rid])) {
            $roles = array_diff($account->roles, array($rid => $role_name));
            user_save($account, array('roles' => $roles));
          }
        }
        $selectall = FALSE;
      }
      else {
        foreach ($accounts as $uid) {
          $account = user_load(array('uid' => (int)$uid));
          // Skip removing the role from the user if they already don't have it.
          if ($account !== FALSE && isset($account->roles[$rid])) {
            $roles = array_diff($account->roles, array($rid => $role_name));
            user_save($account, array('roles' => $roles));
          }
          unset($accounts[$uid]);
        }
      }
    } break;
  }
}

/**
 * Reset the session persistent variables except 'filters'.
 */
function advuser_reset_variables() {
  $advuser = &$_SESSION['advuser'];
  $advuser['accounts'] = array();
  $advuser['selectall'] = FALSE;
  $advuser['deselected'] = array();
}

// vim:ft=php:sts=2:sw=2:ts=2:et:ai:sta:ff=unix
